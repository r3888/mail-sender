#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
var nodemailer = require("nodemailer");


async function send_email(receiver, text){
    const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'kallie57@ethereal.email',
            pass: 'tMWRAmBuuUCSCUUBH1'
        }
    });
    // send mail with defined transport object
    let info = await transporter.sendMail({
    from: '"Bookingn" <noreply@bookingn.com>', // sender address
    to: receiver, // list of receivers
    subject: "New Event", // Subject line
    text: text, // plain text body
    });
    console.log('\t[-] Preview URL: ' + nodemailer.getTestMessageUrl(info));
    return 0;
}

var HOST = "localhost";
if(process.env.RABBITMQ_HOST) HOST = process.env.RABBITMQ_HOST;
var PORT = "5672";
if(process.env.RABBITMQ_PORT) PORT = process.env.RABBITMQ_PORT;
const URL = `amqp://${HOST}:${PORT}`;

amqp.connect(URL, function(error0, connection) {
  if (error0) {
    throw error0;
  } 
  
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }

    var queue = 'bookingn_bulk_email';
    channel.assertQueue(queue, {
      durable: false,
    });

    channel.prefetch(1);
    console.log(" [*] Waiting for messages in %s. ", queue);


    channel.consume(queue, async function(msg) {
        var secs = msg.content.toString().split('.').length - 1;    
        console.log(" [x] Received %s", msg.content.toString());
        var obj = JSON.parse(msg.content.toString());
        for(const i in obj.email_addresses){
            console.log(" [x] Sending to "+obj.email_addresses[i]);
            send_email(obj.email_addresses[i], obj.email_text).then(ret => {console.log(" [x] Mail sent, ret val ", ret);}, 
                                                                err => {console.log("[x] Error ", err);})
        }
        
      setTimeout(function() { console.log(" [x] Done"); channel.ack(msg); }, secs * 1000);
    }, 
    {
      noAck: false
    });
  });
});

